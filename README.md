# How to Test React Components

In an effort to outline how to better test React components, this example was created. Please refer 
to [**How to Develop and Test React Components**](https://medium.com/@joshuavanderen/how-to-test-react-components-ae15cefedee9?sk=ac9b0e93c9896542a3cc33ada993b307) as reference material.

## Access

To access a live running example, visit the following URL: https://jvanderen1.gitlab.io/testing-react-components/

This example has a mock API call for 3 seconds before loading the table. In this table, the user can 
specify the number of rows and columns in the table, along with randomizing the values.

## Usage
The following are required to run this project:

- [Node.js](https://nodejs.org/en/)
- [serve](https://www.npmjs.com/package/serve) (_For production mode only_)

### Run the Application (Development Mode)
To run this application in development mode, it will need access to http://localhost:3000. With 
`react-scripts`, the application with start in development mode with a server, along with hot
reloading.

<details><summary><b>Show instructions</b></summary>

1. Install the required packages outlined in `package.json`:

    ```shell script
    $ npm install
    ```

2. Run the application in development mode:

    ```shell script
    $ npm run start
    ```
   
3. Go to http://localhost:3000 in your web browser.

</details>

### Run the Application (Production Mode)
To run this application in production mode, it will need access to http://localhost:5000. With 
`serve`, the application with start in production mode with a server.

<details><summary><b>Show instructions</b></summary>

1. Install the required packages outlined in `package.json`:

    ```shell script
    $ npm install
    ```

2. Build the application in production mode:

    ```shell script
    $ npm run build:local
    ```
   
   **For CI/CD pipeline**, the application's production build will reference a
   different `PUBLIC_URL` in `.env`.
   
   Build the application in production mode for `PUBLIC_URL` in `.env`:
      
   ```shell script
   $ npm run build
   ```

3. Serve the application at http://localhost:5000. The easiest way to achieve this is
with `serve`:

   ```shell script
   $ npm install -g serve
   $ serve -s build
   ```
   
4. Go to http://localhost:5000 in your web browser.
   
</details>

### Run the Application Tests

#### Unit Tests

This project contains unit tests for the React components. Both snapshot testing and functional 
tests are run to achieve near full coverage.

<details><summary><b>Show instructions</b></summary>

1. Install the required packages outlined in `package.json`:

    ```shell script
    $ npm install
    ```

2. Run the application's unit tests without a coverage report:

    ```shell script
    $ npm run test
    ```
   
   Or run the application's unit tests with a coverage report:
   
   ```shell script
   $ npm run test:coverage
   ```
   
3. **For CI/CD pipeline**, the application's unit tests should be non-blocking. 

   Run the application's unit tests with a coverage report in non-blocking mode:
   
   ```shell script
   $ npm run test:coverage:ci
   ```

</details>

#### Code Analysis Tests

The project also contains code analysis tests through linting. 

<details><summary><b>Show instructions</b></summary>

1. Install the required packages outlined in `package.json`:

    ```shell script
    $ npm install
    ```

2. Run `eslint` on the application's source code:

    ```shell script
    $ npm run lint
    ```
   
   Run `eslint` with the `--fix` flag to potentially fix linting issues:
   
   ```shell script
   $ npm run lint:fix
   ```

</details>

## Acknowledgements

1. [React — Composing Higher-Order Components (HOCs)](https://medium.com/dailyjs/react-composing-higher-order-components-hocs-3a5288e78f55 "Title")
 by [Casey Morris](https://medium.com/@caseymorrisus)
