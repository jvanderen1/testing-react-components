// Internal
// --------

const datastoreMock = {
  loggedIn: true,
  numRows: 10,
  numCols: 10,
  userName: 'Joshua Van Deren',
};

// Exports
// -------

export default datastoreMock;
