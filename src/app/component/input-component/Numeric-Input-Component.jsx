// Imports
// -------

// Libraries
import React from 'react';
import PropTypes from 'prop-types';
// Components
import ButtonComponent from '../button-component/Button-Component';

// Internal
// --------

class NumericInputComponent extends React.PureComponent {
  //
  // Instance methods

  handleClick = () => {
    const {
      onClick,
    } = this.props;

    const {
      value,
    } = this.state;

    onClick(parseInt(value, 10));
  };

  handleChange = (event) => {
    this.setState({
      value: event.target.value,
    });
  };

  updateDefaultValueIfNeeded = (prevDefaultValue) => {
    const {
      defaultValue,
    } = this.props;

    if (prevDefaultValue !== defaultValue) {
      this.setState({
        value: defaultValue,
      });
    }
  };

  //
  // Lifecycle methods

  constructor(props) {
    super(props);
    this.state = {
      value: props.defaultValue,
    };
  }

  componentDidUpdate(prevProps) {
    this.updateDefaultValueIfNeeded(prevProps.defaultValue);
  }

  render() {
    const {
      buttonLabel,
      placeholder,
    } = this.props;

    const {
      value,
    } = this.state;

    return (
      <form>
        <input type="number" placeholder={placeholder} value={value} onChange={this.handleChange} />
        <ButtonComponent label={buttonLabel} onClick={this.handleClick} />
      </form>
    );
  }
}

//
// Static properties

NumericInputComponent.propTypes = {
  buttonLabel: PropTypes.string,
  defaultValue: PropTypes.number,
  placeholder: PropTypes.string,
  onClick: PropTypes.func.isRequired,
};

NumericInputComponent.defaultProps = {
  buttonLabel: 'Click me',
  defaultValue: 0,
  placeholder: 'Please enter a number...',
};

// Exports
// -------

// Default export
export default NumericInputComponent;
