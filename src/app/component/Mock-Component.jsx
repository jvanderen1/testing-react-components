// Imports
// -------
// Libraries
import React from 'react';
import PropTypes from 'prop-types';

// Internal
// --------

const MockComponent = ({ children, ...passedProps }) => (
  <div {...passedProps}>
    {children}
  </div>
);

//
// Static properties

MockComponent.propTypes = {
  children: PropTypes.node,
};

MockComponent.defaultProps = {
  children: null,
};

// Exports
// -------

export default MockComponent;
