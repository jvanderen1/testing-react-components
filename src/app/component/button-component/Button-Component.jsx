// Imports
// -------

// Libraries
import React from 'react';
import PropTypes from 'prop-types';

// Internal
// --------

class ButtonComponent extends React.PureComponent {
  //
  // Lifecycle methods

  render() {
    const {
      label,
      onClick,
      type,
    } = this.props;

    return (
      <button onClick={onClick} type={type}>
        {label}
      </button>
    );
  }
}

//
// Static properties

ButtonComponent.propTypes = {
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  type: PropTypes.string,
};

ButtonComponent.defaultProps = {
  type: 'button',
};

// Exports
// -------

export default ButtonComponent;
