// Imports
// -------

// Libraries
import React from 'react';
import PropTypes from 'prop-types';

// Internal
// --------

class ErrorComponent extends React.PureComponent {
  //
  // Lifecycle methods

  render() {
    const {
      errorMessage,
    } = this.props;

    return (
      <div>
        {errorMessage}
      </div>
    );
  }
}

//
// Static properties

ErrorComponent.propTypes = {
  errorMessage: PropTypes.string,
};

ErrorComponent.defaultProps = {
  errorMessage: 'Error!!!',
};

// Exports
// -------

export default ErrorComponent;
