// Imports
// -------

// Libraries
import React from 'react';
import PropTypes from 'prop-types';

// Internal
// --------

class LoadingComponent extends React.PureComponent {
  //
  // Lifecycle methods

  render() {
    const {
      loadingMessage,
    } = this.props;

    return (
      <div>
        {loadingMessage}
      </div>
    );
  }
}

LoadingComponent.propTypes = {
  loadingMessage: PropTypes.string,
};

LoadingComponent.defaultProps = {
  loadingMessage: 'Loading...',
};

// Exports
// -------

export default LoadingComponent;
