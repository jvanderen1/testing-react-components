// Imports
// -------

// Libraries
import React from 'react';
import PropTypes from 'prop-types';

// Internal
// --------

class TableCellComponent extends React.PureComponent {
  //
  // Lifecycle methods

  render() {
    const {
      data,
      isBold,
    } = this.props;

    const style = {
      fontWeight: isBold ? 'bold' : '',
    };

    return (
      <td>
        <span style={style}>{data}</span>
      </td>
    );
  }
}

//
// Static properties

TableCellComponent.propTypes = {
  data: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
  isBold: PropTypes.bool,
};

TableCellComponent.defaultProps = {
  isBold: false,
};

// Exports
// -------

// Default export
export default TableCellComponent;
