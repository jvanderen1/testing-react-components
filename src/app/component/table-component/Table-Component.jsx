// Imports
// -------

// Libraries
import React from 'react';
import PropTypes from 'prop-types';

// Internal
// --------

class TableComponent extends React.PureComponent {
  //
  // Lifecycle methods

  render() {
    const {
      children,
    } = this.props;

    return (
      <table>
        <tbody>
          {children}
        </tbody>
      </table>
    );
  }
}

//
// Static properties

TableComponent.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

// Exports
// -------

// Default export
export default TableComponent;
