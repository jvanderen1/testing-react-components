// Imports
// -------

// Libraries
import React from 'react';
import PropTypes from 'prop-types';

// Internal
// --------

class TableRowComponent extends React.PureComponent {
  //
  // Lifecycle methods

  render() {
    const {
      children,
    } = this.props;

    return (
      <tr>
        {children}
      </tr>
    );
  }
}

//
// Static properties

TableRowComponent.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.element,
  ]).isRequired,
};

// Exports
// -------

// Default export
export default TableRowComponent;
