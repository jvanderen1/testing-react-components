// Imports
// -------

// Libraries
import React from 'react';

// Internal
// --------

const displayName = 'DatastoreContext';
const DatastoreContext = React.createContext(displayName);
DatastoreContext.displayName = displayName;

// Exports
// -------

// Default export
export default DatastoreContext;
