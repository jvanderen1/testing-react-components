// Imports
// -------

// Libraries
import React from 'react';
// Context
import Context from './Datastore-Context';

// Internal
// --------

const withDatastoreConsumer = (WrappedComponent) => {
  const DatastoreConsumer = (props) => (
    <Context.Consumer>
      {(contextProps) => (
        <WrappedComponent {...props} {...contextProps} />
      )}
    </Context.Consumer>
  );

  return DatastoreConsumer;
};

// Exports
// -------

export default withDatastoreConsumer;
