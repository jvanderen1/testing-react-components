// Imports
// -------

// Libraries
import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
// Context
import Context from './Datastore-Context';
// HOCs
import hasMockData from '../../hoc/hasMockData';
// Mocks
import datastoreMock from '../../../mocks/datastoreMock';
// Utilities
import isEmpty from '../../util/isEmpty';

// Internal
// --------

const delay = 3000; // for mocking purposes.

//

class DatastoreProvider extends React.PureComponent {
  //
  // Instance methods

  getValue = () => {
    const {
      isLoading,
      hasError,
    } = this.props;

    return {
      ...this.state,
      isLoading,
      hasError,
      setNumCols: this.handleSetNumCols,
      setNumRows: this.handleSetNumRows,
    };
  };

  handleSetNumCols = (numCols) => {
    this.setState({ numCols });
  };

  handleSetNumRows = (numRows) => {
    this.setState({ numRows });
  };

  updateDataIfNeeded = (prevData) => {
    const {
      data,
    } = this.props;

    if (!_.isEqual(prevData, data)) {
      this.setState(data);
    }
  };

  //
  // Lifecycle methods

  constructor(props) {
    super(props);
    this.state = isEmpty(props.data) ? {} : props.data;
  }

  componentDidUpdate(prevProps) {
    this.updateDataIfNeeded(prevProps.data);
  }

  render() {
    const {
      children,
    } = this.props;

    return (
      <Context.Provider value={this.getValue()}>
        {children}
      </Context.Provider>
    );
  }
}

//
// Static properties

DatastoreProvider.propTypes = {
  children: PropTypes.element.isRequired,
  data: PropTypes.oneOfType([
    () => null,
    PropTypes.shape({
      loggedIn: PropTypes.bool,
      numRows: PropTypes.number,
      numCols: PropTypes.number,
      userName: PropTypes.string,
    }),
  ]).isRequired,
  isLoading: PropTypes.bool, // from hasMockData
  hasError: PropTypes.bool, // from hasMockData
};

DatastoreProvider.defaultProps = {
  isLoading: false,
  hasError: false,
};

// Exports
// -------

export {
  DatastoreProvider,
};

// Default export
export default (
  hasMockData(datastoreMock, delay)(
    DatastoreProvider,
  )
);
