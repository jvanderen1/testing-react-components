// Internal
// --------

const randomHelper = (min, max) => (
  Math.floor(Math.random() * (max - min) + min)
);

// Exports
// -------

export default randomHelper;
