// Internal
// --------

const isEmpty = (variable) => {
  if (variable.constructor === Object) {
    return Object.keys(variable).length === 0;
  }

  if (variable.constructor === Array) {
    return variable.length === 0;
  }

  return false;
};

// Exports
// -------

export default isEmpty;
