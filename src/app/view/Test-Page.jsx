// Imports
// -------

// Libraries
import React from 'react';
import PropTypes from 'prop-types';
// Components
import ButtonComponent from '../component/button-component/Button-Component';
import NumericInputComponent from '../component/input-component/Numeric-Input-Component';
import TableCellComponent from '../component/table-component/Table-Cell-Component';
import TableComponent from '../component/table-component/Table-Component';
import TableRowComponent from '../component/table-component/Table-Row-Component';
// Context
import withDatastore from '../context/datastore/Datastore-Consumer';
// HOCs
import hasError from '../hoc/hasError';
import hasLoader from '../hoc/hasLoader';
// Utilities
import randomHelper from '../util/randomHelper';

// Internal
// --------

class TestPage extends React.PureComponent {
  //
  // Instance methods

  fillTable = () => {
    const {
      tableValues,
    } = this.state;

    return (
      <TableComponent>
        {tableValues.map((row, rowIdx) => TestPage.getRowComponent(row, rowIdx))}
      </TableComponent>
    );
  };

  handleRandomizeValues = () => {
    const {
      numCols,
      numRows,
    } = this.props;

    this.handleSetTableValues({ numCols, numRows });
  };

  handleSetNumCols = (value) => {
    const {
      numCols,
      numRows,
      setNumCols,
    } = this.props;

    if (value !== numCols) {
      this.handleSetTableValues({ numCols: value, numRows });
      setNumCols(value);
    }
  };

  handleSetNumRows = (value) => {
    const {
      numCols,
      numRows,
      setNumRows,
    } = this.props;

    if (value !== numRows) {
      this.handleSetTableValues({ numCols, numRows: value });
      setNumRows(value);
    }
  };

  handleSetTableValues = ({ numCols, numRows }) => {
    this.setState({
      tableValues: TestPage.getTableValues({ numCols, numRows }),
    });
  };

  updateTableValuesIfNeeded = (prevNumCols, prevNumRows) => {
    const {
      numCols,
      numRows,
    } = this.props;

    if (prevNumCols !== numCols || prevNumRows !== numRows) {
      this.handleSetTableValues({ numCols, numRows });
    }
  };

  //
  // Lifecycle methods

  constructor(props) {
    super(props);
    this.state = {
      tableValues: TestPage.getTableValues(props),
    };
  }

  componentDidUpdate(prevProps) {
    this.updateTableValuesIfNeeded(prevProps.numCols, prevProps.numRows);
  }

  render() {
    const {
      numCols,
      numRows,
    } = this.props;

    const {
      tableValues,
    } = this.state;

    return (
      <div id="test-page">
        {
          !TestPage.isArrayEmpty(tableValues)
          && this.fillTable()
        }
        <ButtonComponent
          label="Randomize"
          onClick={this.handleRandomizeValues}
        />
        <NumericInputComponent
          buttonLabel="Set columns"
          defaultValue={numCols}
          onClick={this.handleSetNumCols}
        />
        <NumericInputComponent
          buttonLabel="Set rows"
          defaultValue={numRows}
          onClick={this.handleSetNumRows}
        />
      </div>
    );
  }
}

//
// Static properties

TestPage.propTypes = {
  numCols: PropTypes.number, // from withDatastore
  numRows: PropTypes.number, // from withDatastore
  setNumCols: PropTypes.func, // from withDatastore
  setNumRows: PropTypes.func, // from withDatastore
};

TestPage.defaultProps = {
  numCols: 0,
  numRows: 0,
  setNumCols: () => null,
  setNumRows: () => null,
};

//
// Static methods

TestPage.isArrayEmpty = (arr) => !(arr.length > 0 && arr[0].length > 0);

TestPage.getCellComponent = (data, rowIdx, colIdx) => {
  const rowEven = rowIdx % 2 === 0;
  const colEven = colIdx % 2 === 0;
  const isBold = (rowEven && !colEven) || (!rowEven && colEven);

  return (
    <TableCellComponent key={colIdx} data={data} isBold={isBold} />
  );
};

TestPage.getRowComponent = (row, rowIdx) => (
  <TableRowComponent key={rowIdx}>
    {row.map((data, colIdx) => TestPage.getCellComponent(data, rowIdx, colIdx))}
  </TableRowComponent>
);

TestPage.getTableValues = ({ numCols, numRows }) => Array.from({ length: numRows }, () => (
  Array.from({ length: numCols }, () => (
    randomHelper(0, 10)
  ))
));

// Exports
// -------

export {
  TestPage,
};

// Default export
export default (
  withDatastore(
    hasError()(
      hasLoader()(TestPage),
    ),
  )
);
