// Imports
// -------
import React from 'react';

// Internal
// --------

const hasProps = (injectedProps) => (WrappedComponent) => (
  (props) => (
    <WrappedComponent {...injectedProps} {...props} />
  )
);

// Exports
// -------

export default hasProps;
