// Imports
// -------
// Libraries
import React from 'react';

// Internal
// --------

const hasBranch = (branchTest, ComponentOnPass, ComponentOnFail) => (props) => (
  (branchTest && <ComponentOnPass {...props} />) || <ComponentOnFail {...props} />
);

// Exports
// -------

export default hasBranch;
