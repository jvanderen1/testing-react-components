// Imports
// -------
import React from 'react';

// Internal
// --------

const hasLogger = (prefix = '') => (WrappedComponent) => (
  (props) => {
    console.log(`${prefix}[Props]:`, props); // eslint-disable-line no-console
    return (
      <WrappedComponent {...props} />
    );
  }
);

// Exports
// -------

export default hasLogger;
