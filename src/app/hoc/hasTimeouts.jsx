// Imports
// -------
// Libraries
import React from 'react';

// Internal
// --------

const hasTimeouts = (WrappedComponent) => {
  const HasTimeouts = (props) => {
    const timeouts = [];

    const addTimeout = (func, delay) => (
      new Promise((resolve) => {
        const id = setTimeout(resolve, delay);
        timeouts.push(id);
      }).then(() => (
        func()
      ))
    );

    const clearTimeouts = () => {
      timeouts.forEach(clearTimeout);
    };

    return (
      <WrappedComponent
        addTimeout={addTimeout}
        clearTimeouts={clearTimeouts}
        {...props}
      />
    );
  };

  return HasTimeouts;
};

// Exports
// -------

export default hasTimeouts;
