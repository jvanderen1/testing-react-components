// Imports
// -------
// Libraries
import PropTypes from 'prop-types';
// Components
import DefaultErrorComponent from '../component/status-component/Error-Component';
// HOCs
import hasBranch from './hasBranch';

// Internal
// --------

const hasError = (ErrorComponent = DefaultErrorComponent) => (WrappedComponent) => {
  const HasError = (props) => (
    hasBranch(props.hasError, ErrorComponent, WrappedComponent)(props)
  );

  //
  // Static properties

  HasError.propTypes = {
    hasError: PropTypes.bool,
  };

  HasError.defaultProps = {
    hasError: false,
  };

  return HasError;
};

// Exports
// -------

export default hasError;
