// Imports
// -------
// Libraries
import React from 'react';
import PropTypes from 'prop-types';
// HOCs
import hasTimeouts from './hasTimeouts';

// Internal
// --------

const hasMockData = (mockData, delay, throwError = false) => (WrappedComponent) => {
  class HasMockData extends React.Component {
    //
    // Instance methods

    handleSetStateAfterMount = () => {
      const newState = {
        data: mockData,
        hasError: throwError,
        isLoading: false,
      };

      this.setState(newState);
    };

    //
    // Lifecycle methods

    constructor(props) {
      super(props);
      this.state = {
        data: [],
        hasError: false,
        isLoading: true,
      };
    }

    componentDidMount() {
      const {
        addTimeout,
      } = this.props;

      addTimeout(this.handleSetStateAfterMount, delay);
    }

    componentWillUnmount() {
      const {
        clearTimeouts,
      } = this.props;

      clearTimeouts();
    }

    render() {
      const {
        data,
        hasError,
        isLoading,
      } = this.state;

      return (
        <WrappedComponent
          data={data}
          hasError={hasError}
          isLoading={isLoading}
          {...this.props}
        />
      );
    }
  }

  //
  // Static properties

  HasMockData.propTypes = {
    addTimeout: PropTypes.func, // from hasTimeouts
    clearTimeouts: PropTypes.func, // from hasTimeouts
  };

  HasMockData.defaultProps = {
    addTimeout: () => null,
    clearTimeouts: () => null,
  };

  return hasTimeouts(HasMockData);
};

// Exports
// -------

export default hasMockData;
