// Imports
// -------
// Libraries
import PropTypes from 'prop-types';
// Components
import DefaultLoadingComponent from '../component/status-component/Loading-Component';
// HOCs
import hasBranch from './hasBranch';
import hasProps from './hasProps';

// Internal
// --------

const hasLoader = (LoadingComponent = DefaultLoadingComponent) => (WrappedComponent) => {
  const HasLoader = (props) => (
    hasBranch(
      props.isLoading,
      hasProps({ loadingMessage: props.loadingMessage })(LoadingComponent),
      WrappedComponent,
    )(props)
  );

  //
  // Static properties

  HasLoader.propTypes = {
    isLoading: PropTypes.bool,
  };

  HasLoader.defaultProps = {
    isLoading: false,
  };

  return HasLoader;
};

// Exports
// -------

export default hasLoader;
