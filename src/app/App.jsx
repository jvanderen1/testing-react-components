// Imports
// -------

// Libraries
import React from 'react';
// Component
import TestPage from './view/Test-Page';
// Context
import DatastoreProvider from './context/datastore/Datastore-Provider';

// Internal
// --------

class App extends React.PureComponent {
  //
  // Lifecycle methods

  render() {
    return (
      <DatastoreProvider>
        <TestPage />
      </DatastoreProvider>
    );
  }
}

// Exports
// -------

// Default Export
export default App;
