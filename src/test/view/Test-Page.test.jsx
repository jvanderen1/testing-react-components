// Imports
// -------

// Libraries
import React from 'react';
import renderer from 'react-test-renderer';
// Components
import { TestPage } from '../../app/view/Test-Page';

// Mocks
// -----

jest.mock('app/component/button-component/Button-Component', () => 'ButtonComponent');
jest.mock('app/component/input-component/Numeric-Input-Component', () => 'NumericInputComponent');
jest.mock('app/component/table-component/Table-Cell-Component', () => 'TableCellComponent');
jest.mock('app/component/table-component/Table-Component', () => 'TableComponent');
jest.mock('app/component/table-component/Table-Row-Component', () => 'TableRowComponent');

jest.spyOn(global.Math, 'random').mockReturnValue(0);

// Internal
// --------

const defaultProps = {
  numCols: 2,
  numRows: 2,
  setNumRows: jest.fn(),
  setNumCols: jest.fn(),
};

//

const getComponent = (props) => renderer.create(
  <TestPage {...{ ...defaultProps, ...props }} />,
);

// Tests
// -----

describe('app/view/Test-Page', () => {
  let component;

  describe('Rendering', () => {
    it('renders default correctly', () => {
      component = getComponent();
      expect(component).toMatchSnapshot();
    });

    it('renders no data correctly', () => {
      component = getComponent({ numCols: null, numRows: null });
      expect(component).toMatchSnapshot();
    });
  });

  describe('Lifecycle', () => {
    let instance;
    let newComponent;
    const updateComponent = (props) => (
      <TestPage {...{ ...defaultProps, ...props }} />
    );

    beforeEach(() => {
      component = getComponent();
      instance = component.root.instance;
      instance.handleSetTableValues = jest.fn();
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    it('updates table values when `numCols` changes', () => {
      newComponent = updateComponent({ numCols: 0 });
      component.update(newComponent);
      expect(instance.handleSetTableValues).toHaveBeenCalled();
    });

    it('updates table values when `numRows` changes', () => {
      newComponent = updateComponent({ numRows: 0 });
      component.update(newComponent);
      expect(instance.handleSetTableValues).toHaveBeenCalled();
    });

    it('does not update table values when neither `numCols` or `numRows` changes', () => {
      newComponent = updateComponent();
      component.update(newComponent);
      expect(instance.handleSetTableValues).not.toHaveBeenCalled();
    });
  });

  describe('Interaction', () => {
    let root;
    let instance;

    beforeEach(() => {
      component = getComponent();
      root = component.root;
      instance = root.instance;
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    describe('Set columns button', () => {
      let numericInputComponent;

      beforeEach(() => {
        [ numericInputComponent ] = root.findAllByType('NumericInputComponent');
      });

      it('handles `handleSetNumCols` with change in columns correctly', () => {
        const numCols = 1;
        const value = { tableValues: [ [ 0 ], [ 0 ] ] };

        instance.setState = jest.fn();
        numericInputComponent.props.onClick(numCols);

        expect(instance.setState).toHaveBeenCalledWith(value);
        expect(instance.props.setNumCols).toHaveBeenCalledWith(numCols);
      });

      it('handles `handleSetNumCols` with no change in columns correctly', () => {
        const numCols = 2;

        instance.setState = jest.fn();
        numericInputComponent.props.onClick(numCols);

        expect(instance.setState).not.toHaveBeenCalled();
        expect(instance.props.setNumCols).not.toHaveBeenCalled();
      });
    });

    describe('Set rows button', () => {
      let numericInputComponent;

      beforeEach(() => {
        [ , numericInputComponent ] = root.findAllByType('NumericInputComponent');
      });

      it('handles `handleSetNumRows` with change in rows correctly', () => {
        const numRows = 1;
        const value = { tableValues: [ [ 0, 0 ] ] };

        instance.setState = jest.fn();
        numericInputComponent.props.onClick(numRows);

        expect(instance.setState).toHaveBeenCalledWith(value);
        expect(instance.props.setNumRows).toHaveBeenCalledWith(numRows);
      });

      it('handles `handleSetNumRows` with no change in rows correctly', () => {
        const numRows = 2;

        instance.setState = jest.fn();
        numericInputComponent.props.onClick(numRows);

        expect(instance.setState).not.toHaveBeenCalled();
        expect(instance.props.setNumRows).not.toHaveBeenCalled();
      });
    });

    describe('Randomize button', () => {
      let buttonComponent;

      beforeEach(() => {
        buttonComponent = root.findByType('ButtonComponent');
      });

      it('handles `setTableValues` correctly', () => {
        const value = { tableValues: [ [ 0, 0 ], [ 0, 0 ] ] };
        instance.setState = jest.fn();
        buttonComponent.props.onClick();
        expect(instance.setState).toHaveBeenCalledWith(value);
      });
    });
  });
});
