// Imports
// -------

// Libraries
import renderer from 'react-test-renderer';
// Components
import MockComponent from '../../app/component/Mock-Component';
// HOCs
import hasLogger from '../../app/hoc/hasLogger';

// Mocks
// -----

jest.mock('app/component/Mock-Component', () => 'MockComponent');
console.log = jest.fn(); // eslint-disable-line no-console
jest.spyOn(console, 'log');

// Internal
// --------

const defaultProps = {
  aNumber: 1,
  aString: 'test',
};

const getComponent = (props) => renderer.create(
  hasLogger('Prefix')(MockComponent)({ ...props, ...defaultProps }),
);

// Tests
// -----

describe('app/hoc/hasLogger', () => {
  describe('Rendering', () => {
    it('calls `console.log` correctly', () => {
      getComponent();
      expect(console.log.mock.calls[0][0]).toBe('Prefix[Props]:'); // eslint-disable-line no-console
      // TODO: Should be better way to do this…
      expect(console.log.mock.calls[0][1].length).not.toEqual(0); // eslint-disable-line no-console
    });
  });
});
