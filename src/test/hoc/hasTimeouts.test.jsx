// Imports
// -------

// Libraries
import renderer from 'react-test-renderer';
// Components
import MockComponent from '../../app/component/Mock-Component';
// HOCs
import hasTimeouts from '../../app/hoc/hasTimeouts';

// Mocks
// -----

jest.mock('app/component/Mock-Component', () => 'MockComponent');
jest.useFakeTimers();

// Internal
// --------

const getComponent = (props) => renderer.create(
  hasTimeouts(MockComponent)(props),
);

// Tests
// -----

describe('app/hoc/hasTimeouts', () => {
  let component;

  describe('Rendering', () => {
    it('renders `MockComponent` with timeouts', () => {
      component = getComponent();
      expect(component).toMatchSnapshot();
    });
  });

  describe('Interaction', () => {
    let root;

    beforeEach(() => {
      component = getComponent();
      root = component.root;
    });

    describe('addTimeout', () => {
      it('calls provided `func` after `delay` properly', async () => {
        const delay = 1000;
        const value = 42;
        const mockFn = () => value;
        const promise = root.props.addTimeout(mockFn, delay);
        jest.advanceTimersByTime(delay);
        await expect(promise).resolves.toBe(value);
      });
    });

    describe('clearTimeouts', () => {
      it('calls `clearTimeout` properly', async () => {
        const delay = 1000;
        const mockFn = jest.fn();
        const promise = root.props.addTimeout(mockFn, delay);
        jest.advanceTimersByTime(delay);
        expect(clearTimeout).not.toHaveBeenCalled();
        await promise.then(() => {
          root.props.clearTimeouts();
          expect(clearTimeout).toHaveBeenCalledTimes(1);
        });
      });
    });
  });
});
