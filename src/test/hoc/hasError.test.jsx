// Imports
// -------

// Libraries
import renderer from 'react-test-renderer';
// Components
import MockComponent from '../../app/component/Mock-Component';
// HOCs
import hasError from '../../app/hoc/hasError';

// Mocks
// -----

jest.mock('app/component/Mock-Component', () => 'MockComponent');

// Internal
// --------

const defaultProps = {
  hasError: true,
};

const getComponent = (props) => renderer.create(
  hasError()(MockComponent)({ ...{ ...defaultProps, ...props } }),
);

// Tests
// -----

describe('app/hoc/hasError', () => {
  let component;

  describe('Rendering', () => {
    it('renders default error component when `hasError` is true', () => {
      component = getComponent();
      expect(component).toMatchSnapshot();
    });

    it('renders `MockComponent` when `hasError` is false', () => {
      component = getComponent({
        hasError: false,
      });
      expect(component).toMatchSnapshot();
    });
  });
});
