// Imports
// -------

// Libraries
import React from 'react';
import renderer from 'react-test-renderer';
// Components
import MockComponent from '../../app/component/Mock-Component';
// HOCs
import hasBranch from '../../app/hoc/hasBranch';

// Mocks
// -----

jest.mock('app/component/Mock-Component', () => 'MockComponent');

// Internal
// --------

const firstComponent = () => (
  <MockComponent mockProps="First" />
);

const secondComponent = () => (
  <MockComponent mockProps="Second" />
);

const getComponent = (branchTest) => renderer.create(
  hasBranch(branchTest, firstComponent, secondComponent)(),
);

// Tests
// -----

describe('app/hoc/hasBranch', () => {
  let component;

  describe('Rendering', () => {
    it('renders `First` when `branchTest` is true', () => {
      component = getComponent(true);
      expect(component).toMatchSnapshot();
    });

    it('renders `Second` when `branchTest` is false', () => {
      component = getComponent(false);
      expect(component).toMatchSnapshot();
    });

    it('renders `Second` when `branchTest` does not exist', () => {
      component = getComponent();
      expect(component).toMatchSnapshot();
    });
  });
});
