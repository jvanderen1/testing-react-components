// Imports
// -------

// Libraries
import renderer from 'react-test-renderer';
// Components
import MockComponent from '../../app/component/Mock-Component';
// HOCs
import hasMockData from '../../app/hoc/hasMockData';
// Mocks
import datastoreMock from '../../mocks/datastoreMock';

// Mocks
// -----

jest.mock('app/component/Mock-Component', () => 'MockComponent');

// Internal
// --------

const defaultValues = {
  data: datastoreMock,
  delay: 1000,
  hasError: false,
};

const getComponent = (props, values = {}) => {
  const {
    data,
    delay,
    hasError,
  } = { ...defaultValues, ...values };

  return renderer.create(
    hasMockData(data, delay, hasError)(MockComponent)(props),
  );
};

// Tests
// -----

describe('app/hoc/hasMockData', () => {
  let component;

  describe('Rendering', () => {
    it('renders `MockComponent` with timeouts', () => {
      component = getComponent();
      expect(component).toMatchSnapshot();
    });
  });

  describe('Lifecycle', () => {
    let instance;

    describe('componentDidMount', () => {
      it('sets state properly during `componentDidMount` with `hasError` set to false', () => {
        component = getComponent({
          addTimeout: (fn) => fn(),
        });
        instance = component.root.instance;
        expect(instance.state).toEqual({
          data: defaultValues.data,
          hasError: false,
          isLoading: false,
        });
      });

      it('sets state properly during `componentDidMount` with `hasError` set to true', () => {
        // TODO: Maybe there's a better way to test this with timeout
        component = getComponent({
          addTimeout: (fn) => fn(),
        }, {
          hasError: true,
        });
        instance = component.root.instance;
        expect(instance.state).toEqual({
          data: defaultValues.data,
          hasError: true,
          isLoading: false,
        });
      });
    });

    describe('componentWillUnmount', () => {
      it('calls `clearTimeouts` during `componentDidMount`', async () => {
        const mockFn = jest.fn();
        component = getComponent({
          clearTimeouts: () => mockFn(),
        });
        expect(mockFn).not.toHaveBeenCalled();
        component.unmount();
        expect(mockFn).toHaveBeenCalled();
      });
    });
  });
});
