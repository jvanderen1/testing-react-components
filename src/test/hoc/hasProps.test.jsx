// Imports
// -------

// Libraries
import renderer from 'react-test-renderer';
// Components
import MockComponent from '../../app/component/Mock-Component';
// HOCs
import hasProps from '../../app/hoc/hasProps';

// Mocks
// -----

jest.mock('app/component/Mock-Component', () => 'MockComponent');

// Internal
// --------

const defaultProps = {
  aNumber: 1,
  aString: 'test',
};

const getComponent = (props) => renderer.create(
  hasProps({ ...defaultProps, ...props })(MockComponent)(),
);

// Tests
// -----

describe('app/hoc/hasProps', () => {
  let component;

  describe('Rendering', () => {
    it('renders `MockComponent` with injected props', () => {
      component = getComponent();
      expect(component).toMatchSnapshot();
    });
  });
});
