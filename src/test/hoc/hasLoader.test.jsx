// Imports
// -------

// Libraries
import renderer from 'react-test-renderer';
// Components
import MockComponent from '../../app/component/Mock-Component';
// HOCs
import hasLoader from '../../app/hoc/hasLoader';

// Mocks
// -----

jest.mock('app/component/Mock-Component', () => 'MockComponent');

// Internal
// --------

const defaultProps = {
  isLoading: true,
};

const getComponent = (props) => renderer.create(
  hasLoader()(MockComponent)({ ...{ ...defaultProps, ...props } }),
);

// Tests
// -----

describe('app/hoc/hasLoader', () => {
  let component;

  describe('Rendering', () => {
    it('renders default loading component when `isLoading` is true', () => {
      component = getComponent();
      expect(component).toMatchSnapshot();
    });

    it('renders `MockComponent` when `isLoading` is false', () => {
      component = getComponent({
        isLoading: false,
      });
      expect(component).toMatchSnapshot();
    });
  });
});
