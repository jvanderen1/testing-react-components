// Imports
// -------

// Libraries
import React from 'react';
import renderer from 'react-test-renderer';
// Components
import App from '../app/App';

// Mocks
// -----

jest.mock('app/view/Test-Page', () => 'TestPage');
jest.mock('app/context/datastore/Datastore-Provider', () => 'DatastoreProvider');

// Internal
// --------

const getComponent = (props) => renderer.create(
  <App {...props} />,
);

// Tests
// -----

describe('app/App', () => {
  let component;

  describe('Rendering', () => {
    it('renders default correctly', () => {
      component = getComponent();
      expect(component).toMatchSnapshot();
    });
  });
});
