// Imports
// -------
// Libraries
import renderer from 'react-test-renderer';
// Components
import MockComponent from '../../../app/component/Mock-Component';
// Context
import DatastoreContext from '../../../app/context/datastore/Datastore-Context';
// HOCs
import withDatastoreConsumer from '../../../app/context/datastore/Datastore-Consumer';
// Mocks
import datastoreMock from '../../../mocks/datastoreMock';

// Mocks
// -----

jest.mock('app/component/Mock-Component', () => 'MockComponent');
DatastoreContext.Consumer = jest.fn((props) => props.children(datastoreMock));

// Internal
// --------

const getComponent = () => renderer.create(
  withDatastoreConsumer(MockComponent)(),
);

// Tests
// -----

describe('app/context/datastore/Datastore-Context', () => {
  let component;

  describe('Rendering', () => {
    it('renders default correctly', () => {
      component = getComponent();
      expect(component).toMatchSnapshot();
    });
  });
});
