// Imports
// -------

// Components
import DatastoreContext from '../../../app/context/datastore/Datastore-Context';

// Tests
// -----

describe('app/context/datastore/Datastore-Context', () => {
  let context;

  describe('Export', () => {
    it('exports context correctly', () => {
      context = DatastoreContext;
      expect(context.displayName).toBe('DatastoreContext');
    });
  });
});
