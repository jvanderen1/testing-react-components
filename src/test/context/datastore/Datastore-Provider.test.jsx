// Imports
// -------
// Libraries
import React from 'react';
import renderer from 'react-test-renderer';
// Components
import { DatastoreProvider } from '../../../app/context/datastore/Datastore-Provider';
import MockComponent from '../../../app/component/Mock-Component';
// Mocks
import datastoreMock from '../../../mocks/datastoreMock';

// Mocks
// -----

jest.mock('app/component/Mock-Component', () => 'MockComponent');

// Internal
// --------

const defaultProps = {
  data: datastoreMock,
  isLoading: false,
  hasError: false,
};

//

const getComponent = (props) => renderer.create(
  <DatastoreProvider {...{ ...props, ...defaultProps }}>
    <MockComponent />
  </DatastoreProvider>,
);

// Tests
// -----

describe('app/context/datastore/Datastore-Provider', () => {
  let component;

  describe('Rendering', () => {
    it('renders default correctly', () => {
      component = getComponent();
      expect(component).toMatchSnapshot();
    });
  });

  describe('Lifecycle', () => {
    let instance;
    let newComponent;
    const updateComponent = (props) => (
      <DatastoreProvider {...{ ...defaultProps, ...props }}>
        <MockComponent />
      </DatastoreProvider>
    );

    beforeEach(() => {
      component = getComponent();
      instance = component.root.instance;
      instance.setState = jest.fn();
    });

    describe('`componentDidUpdate`', () => {
      it('updates data when any data changes', () => {
        const newData = {
          data: {
            loggedIn: false,
            numRows: null,
            numCols: null,
            userName: '',
          },
        };
        newComponent = updateComponent(newData);
        component.update(newComponent);
        expect(instance.setState).toHaveBeenCalledWith(newData.data);
      });

      it('does not update when no data changes', () => {
        newComponent = updateComponent();
        component.update(newComponent);
        expect(instance.setState).not.toHaveBeenCalled();
      });
    });
  });

  describe('Helper methods', () => {
    beforeEach(() => {
      component = getComponent();
    });

    describe('getValue', () => {
      let instance;
      let value;

      beforeEach(() => {
        instance = component.root.instance;
        value = instance.getValue();
        instance.setState = jest.fn();
      });

      it('provides correct value', () => {
        expect(value).toMatchObject({
          ...defaultProps.data,
          isLoading: false,
          hasError: false,
          setNumCols: expect.any(Function),
          setNumRows: expect.any(Function),
        });
      });

      describe('setter functions returned', () => {
        it('calls `setNumCols` correctly', () => {
          const numCols = 50;
          value.setNumCols(numCols);
          expect(instance.setState).toHaveBeenCalledWith({ numCols });
        });

        it('calls `setNumRows` correctly', () => {
          const numRows = 50;
          value.setNumRows(numRows);
          expect(instance.setState).toHaveBeenCalledWith({ numRows });
        });
      });
    });
  });
});
