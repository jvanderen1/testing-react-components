// Imports
// -------

// Libraries
import React from 'react';
import renderer from 'react-test-renderer';
// Components
import MockComponent from '../../../app/component/Mock-Component';
import TableRowComponent from '../../../app/component/table-component/Table-Row-Component';

// Mocks
// -----

jest.mock('app/component/Mock-Component', () => 'MockComponent');

// Internal
// --------

const getComponent = (props) => renderer.create(
  <TableRowComponent {...props}>
    <MockComponent />
  </TableRowComponent>,
);

// Tests
// -----

describe('app/component/table-component/Table-Row-Component', () => {
  describe('Rendering', () => {
    it('renders default correctly', () => {
      const component = getComponent();
      expect(component).toMatchSnapshot();
    });
  });
});
