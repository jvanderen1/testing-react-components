// Imports
// -------

// Libraries
import React from 'react';
import renderer from 'react-test-renderer';
// Components
import TableCellComponent from '../../../app/component/table-component/Table-Cell-Component';

// Internal
// --------

const defaultProps = {
  data: 123456789,
};

//

const getComponent = (props) => renderer.create(
  <TableCellComponent {...{ ...defaultProps, ...props }} />,
);

// Tests
// -----

describe('app/component/table-component/Table-Cell-Component', () => {
  describe('Rendering', () => {
    it('renders default correctly', () => {
      const component = getComponent();
      expect(component).toMatchSnapshot();
    });

    it('renders `isBold` correctly', () => {
      const component = getComponent({
        isBold: true,
      });
      expect(component).toMatchSnapshot();
    });
  });
});
