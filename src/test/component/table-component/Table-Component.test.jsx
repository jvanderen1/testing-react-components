// Imports
// -------

// Libraries
import React from 'react';
import renderer from 'react-test-renderer';
// Components
import MockComponent from '../../../app/component/Mock-Component';
import TableComponent from '../../../app/component/table-component/Table-Component';

// Mocks
// -----

jest.mock('app/component/Mock-Component', () => 'MockComponent');

// Internal
// --------

const getComponent = (props) => renderer.create(
  <TableComponent {...props}>
    <MockComponent />
  </TableComponent>,
);

// Tests
// -----

describe('app/component/table-component/Table-Component', () => {
  describe('Rendering', () => {
    it('renders default correctly', () => {
      const component = getComponent();
      expect(component).toMatchSnapshot();
    });
  });
});
