// Imports
// -------

// Libraries
import React from 'react';
import renderer from 'react-test-renderer';
// Components
import MockComponent from '../../app/component/Mock-Component';

// Internal
// --------

const getComponent = (props) => renderer.create(
  <MockComponent {...props}>
    <p>This is a test</p>
  </MockComponent>,
);

// Tests
// -----

describe('app/component/Mock-Component', () => {
  let component;

  describe('Rendering', () => {
    it('renders default correctly', () => {
      component = getComponent();
      expect(component).toMatchSnapshot();
    });
  });
});
