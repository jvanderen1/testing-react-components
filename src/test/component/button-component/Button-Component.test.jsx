// Imports
// -------

// Libraries
import React from 'react';
import renderer from 'react-test-renderer';
// Components
import ButtonComponent from '../../../app/component/button-component/Button-Component';

// Internal
// --------

const defaultProps = {
  label: 'Testing button',
  onClick: jest.fn(),
};

//

const getComponent = (props) => renderer.create(
  <ButtonComponent {...{ ...defaultProps, ...props }} />,
);

// Tests
// -----

describe('app/component/button-component/Button-Component', () => {
  let component;

  describe('Rendering', () => {
    it('renders default correctly', () => {
      component = getComponent();
      expect(component).toMatchSnapshot();
    });

    it('renders `type` correctly', () => {
      component = getComponent({
        type: 'submit',
      });
      expect(component).toMatchSnapshot();
    });
  });

  describe('Interaction', () => {
    describe('button', () => {
      let root;
      let button;

      beforeEach(() => {
        component = getComponent();
        root = component.root;
        button = root.findByType('button');
      });

      it('handles `onClick` correctly', () => {
        expect(root.props.onClick).not.toHaveBeenCalled();
        button.props.onClick();
        expect(root.props.onClick).toHaveBeenCalled();
      });
    });
  });
});
