// Imports
// -------
// Libraries
import React from 'react';
import renderer from 'react-test-renderer';
// Components
import NumericInputComponent from '../../../app/component/input-component/Numeric-Input-Component';

// Mocks
// -----

jest.mock('app/component/button-component/Button-Component', () => 'ButtonComponent');

// Internal
// --------

const defaultProps = {
  onClick: jest.fn(),
};

//

const getComponent = (props) => renderer.create(
  <NumericInputComponent {...{ ...defaultProps, ...props }} />,
);

// Tests
// -----

describe('app/component/input-component/Numeric-Input-Component', () => {
  let component;

  describe('Rendering', () => {
    it('renders default correctly', () => {
      component = getComponent();
      expect(component).toMatchSnapshot();
    });
  });

  describe('Lifecycle', () => {
    let instance;
    let newComponent;
    const updateComponent = (props) => (
      <NumericInputComponent {...{ ...defaultProps, ...props }} />
    );

    beforeEach(() => {
      component = getComponent();
      instance = component.root.instance;
      instance.setState = jest.fn();
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    it('updates `value` when `defaultValue` changes', () => {
      const value = { value: 10 };
      newComponent = updateComponent({ defaultValue: 10 });
      component.update(newComponent);
      expect(instance.setState).toHaveBeenCalledWith(value);
    });
  });

  describe('Interaction', () => {
    describe('ButtonComponent', () => {
      let root;
      let buttonComponent;

      beforeEach(() => {
        component = getComponent();
        root = component.root;
        buttonComponent = root.findByType('ButtonComponent');
      });

      it('handles `onClick` correctly', () => {
        expect(root.props.onClick).not.toHaveBeenCalled();
        buttonComponent.props.onClick();
        expect(root.props.onClick).toHaveBeenCalledWith(0);
      });
    });

    describe('input', () => {
      let instance;
      let root;
      let input;

      beforeEach(() => {
        component = getComponent();
        root = component.root;
        instance = root.instance;
        instance.setState = jest.fn();
        input = root.findByType('input');
      });

      afterEach(() => {
        jest.clearAllMocks();
      });

      it('handles `onChange` correctly', () => {
        const value = 5;
        const event = {};
        event.target = {};
        event.target.value = value;

        expect(instance.setState).not.toHaveBeenCalled();
        input.props.onChange(event);
        expect(instance.setState).toHaveBeenCalledWith({ value });
      });
    });
  });
});
