// Imports
// -------

// Libraries
import React from 'react';
import renderer from 'react-test-renderer';
// Components
import LoadingComponent from '../../../app/component/status-component/Loading-Component';

// Internal
// --------

const getComponent = (props) => renderer.create(
  <LoadingComponent {...props} />,
);

// Tests
// -----

describe('app/component/status-component/Loading-Component', () => {
  let component;

  describe('Rendering', () => {
    it('renders default correctly', () => {
      component = getComponent();
      expect(component).toMatchSnapshot();
    });
  });
});
