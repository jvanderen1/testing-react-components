// Imports
// -------

// Libraries
import React from 'react';
import renderer from 'react-test-renderer';
// Components
import ErrorComponent from '../../../app/component/status-component/Error-Component';

// Internal
// --------

const getComponent = (props) => renderer.create(
  <ErrorComponent {...props} />,
);

// Tests
// -----

describe('app/component/status-component/Error-Component', () => {
  let component;

  describe('Rendering', () => {
    it('renders default correctly', () => {
      component = getComponent();
      expect(component).toMatchSnapshot();
    });
  });
});
