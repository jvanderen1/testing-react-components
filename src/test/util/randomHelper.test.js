// Imports
// -------
// Utilities
import randomHelper from '../../app/util/randomHelper';

// Tests
// -----

describe('app/utils/randomHelper', () => {
  let randomValue;
  let testValue;

  describe('Helper function', () => {
    it('returns a random integer between `min` (inclusive) and `max` (not inclusive)', () => {
      const min = 50;
      const max = min + 1;
      randomValue = randomHelper(min, max);
      testValue = randomValue >= min && randomValue < max;
      expect(testValue).toBe(true);
      expect(randomValue).toBe(min);
    });
  });
});
