// Imports
// -------
// Utilities
import isEmpty from '../../app/util/isEmpty';

// Tests
// -----

describe('app/utils/isEmpty', () => {
  let testValue;

  describe('Helper function', () => {
    describe('Objects', () => {
      let object;

      it('detects if empty object is empty correctly', () => {
        object = {};
        testValue = isEmpty(object);
        expect(testValue).toBe(true);
      });

      it('detects if non-empty object is not empty correctly', () => {
        object = { a: 1, b: 'test' };
        testValue = isEmpty(object);
        expect(testValue).toBe(false);
      });
    });

    describe('Arrays', () => {
      let array;

      it('detects if empty array is empty correctly', () => {
        array = [];
        testValue = isEmpty(array);
        expect(testValue).toBe(true);
      });

      it('detects if non-empty array is not empty correctly', () => {
        array = [ 1, 'test' ];
        testValue = isEmpty(array);
        expect(testValue).toBe(false);
      });
    });

    describe('Unknown', () => {
      let unknown;

      it('returns false if unknown data type is passed', () => {
        unknown = jest.fn();
        testValue = isEmpty(unknown);
        expect(testValue).toBe(false);
      });
    });
  });
});
